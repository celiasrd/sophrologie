<?php
require_once 'database.php';

$page_title = "Cabinet";

require_once 'layout/header.php';?>

<nav class="nav_header">

    <ul>
        <li><a href="index.php"><img src="images/logo.png"></a></li>
        <li><a href="quisuije.php">Qui suis-je?</a></li>
        <li><a href="cabinet.php">Le Cabinet</a></li>
        <li><a href="presentation.php">Présentation de la sophrologie</a></li>
        <li><a href="evenement.php">Évènements</a></li>
        <li><a href="contact.php">Contact</a></li>
    </ul>


</nav>

<?php require_once 'layout/footer.php'; ?>