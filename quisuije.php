
<?php
require_once 'database.php';

$page_title = "Sophrologie - Frédérique Caillet Morel";

require_once 'layout/header.php';
?>

<nav class="nav_header">

    <ul>
        <li><a href="index.php"><img src="images/logo.png"></a></li>
        <li><a href="quisuije.php">Qui suis-je?</a></li>
        <li><a href="cabinet.php">Le Cabinet</a></li>
        <li><a href="presentation.php">Présentation de la sophrologie</a></li>
        <li><a href="evenement.php">Évènements</a></li>
        <li><a href="contact.php">Contact</a></li>
    </ul>


</nav>


<div class="imageheader">
    <h1 id="quisuije">Qui suis-je ?</h1>
</div>


<section class="prez">
    <article>
        <div><h2 id="nom">Frédéric Caillet Morel</h2></div>
        <p id="textequisuije">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
            architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem. </p>
        
            
</section>
        
    </article>
    
    <div class="avatar"></div>
    
    <section id="observatoir">
        
        <h2>Observatoire National
            de la Sophrologie</h2>

        
        <p id="ons">L’ONS est une association loi 1901 ouvertes aux sophrologues de sensibilités et d’horizons différents et indépendante.  Son but est de témoigner et d’agir pour une meilleure reconnaissance de la sophrologie comme discipline et comme métier. L’Observatoire national de la sophrologie est reconnu d’intérêt général.</p>

    
        <a id="more" href="#"><div id="read">Read more</div></a>
    
        </section>
    
    

<?php require_once 'layout/footer.php';?>