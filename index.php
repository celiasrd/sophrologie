<?php
require_once 'database.php';

$page_title = "Sophrologie - Frédérique Caillet Morel";

require_once 'layout/header.php';
?>
    
   <div id="owl-demo" class="owl-carousel owl-theme">
 
       <div class="item"><img src="images/testheader.png" alt="1"></div>
  <div class="item"><img src="images/2.png" alt="2"></div>
  <div class="item"><img src="images/3.png" alt="3"></div>
  
  </div>

<div class="header_font">

    <nav class="nav_header home">

                
    <ul>
        <li><a href="index.php"><img src="images/logo.png"></a></li>
        <li><a href="quisuije.php">Qui suis-je?</a></li>
        <li><a href="cabinet.php">Le Cabinet</a></li>
        <li><a href="presentation.php">Présentation de la sophrologie</a></li>
        <li><a href="evenement.php">Évènements</a></li>
        <li><a href="contact.php">Contact</a></li>
    </ul>


    </nav>

    <h1>Cabinet de Sophrologie</h1>
    <p>Pédagogie du bien être, méthode de relaxation psycho-corporelle pour une meilleure connaissance de soi et un épanouissement personnel.</p>


</div>

<div class="info_home page-width">

    <h2>Nos prochains évènements</h2>

    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>


</div>

<section>

    <div class="evenement_home">

        <div class="image_home"></div>
        
        <div class="evenement">
        
        <article>
            <h3>Évènement 1</h3>
            <p>PETIT TEXTE LOREM IPSUM
AFIN DE DECRIRE VOTRE
EVENNEMENT LOREM IPSUM</p>
            
            <p class="date">LUNDI 3 NOVEMBRE
12h-13h </p>
        </article>
        
        <article>
            <h3>Évènement 3</h3>
            <p>PETIT TEXTE LOREM IPSUM
AFIN DE DECRIRE VOTRE
EVENNEMENT LOREM IPSUM</p>
            
            <p class="date">LUNDI 3 NOVEMBRE
12h-13h </p>
        </article>
        
        <article>
            <h3>Évènement 3</h3>
            <p>PETIT TEXTE LOREM IPSUM
AFIN DE DECRIRE VOTRE
EVENNEMENT LOREM IPSUM</p>
            
            <p class="date">LUNDI 3 NOVEMBRE
12h-13h </p>
        </article>
            
            <a href="evenement" class="more-event">Plus d'évènement</a>
            
        </div>

    </div>


</section>




<?php require_once 'layout/footer.php'; ?>