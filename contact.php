<?php
require_once 'database.php';

$page_title = "Contact";

require_once 'layout/header.php';
?>

<nav class="nav_header">

    <ul>
        <li><a href="index.php"><img src="images/logo.png"></a></li>
        <li><a href="quisuije.php">Qui suis-je?</a></li>
        <li><a href="cabinet.php">Le Cabinet</a></li>
        <li><a href="presentation.php">Présentation de la sophrologie</a></li>
        <li><a href="evenement.php">Évènements</a></li>
        <li><a href="contact.php">Contact</a></li>
    </ul>


</nav>

<div class="imageheadercontact">
    <h1 id="contact">contact</h1>
</div>

<section>
<img class="photocontact" src="images/merprez.png">
<img class="photocontact" src="images/CONTACTPHRASE.png">
<img class="photocontact" src="images/yogaprez.png">
</section>

<section>
<img class="photocontact" src="images/treecontact.png">  

<div id="infocontact">
    <h1 class="gauchetext">Frédérique Caillet-Morel</h1>
    
    <div id="prezsophro">
    <h3 class="gauchetext">sophrologue</h3>
<p class="gauchetext">
Diplomée de l’Ecole Supérieure 
de Sophrologie Caycédienne 
de l’Ouest et de Paris Ile-de-France

</p>

    </div>
<p class="droitetext">1 Boulevard de la liberté
35220 CHATEAUBOURG

06 77 85 80 63
frederique.cailletmorel@laposte.net</p>

</div>


    
    
</section>

<section id="sites">
    
    <h1 id="sitetitre">Sites de sophrologie</h1>
    <p id="liens">
         
<a href="#">http://www.sophrologie-info.com/</a>
<a href="#">http://www.sophrologie-info.com/</a>
<a href="#">http://www.sophrologie-info.com/</a>
<a href="#">http://www.sophrologie-info.com/</a>
<a href="#">http://www.sophrologie-info.com/</a>
<a href="#">http://www.sophrologie-info.com/</a>

</p>
</section>



<?php require_once 'layout/footer.php'; ?>