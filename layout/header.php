<?php require_once 'database.php'; ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Sophrologie">

        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">



        <title><?php echo $page_title; ?></title>

        <link rel="stylesheet" href="css/normalize.css">

        <!-- Important Owl stylesheet -->
        <link rel="stylesheet" href="owl-carousel/owl.carousel.css">

        <!-- Default Theme -->
        <link rel="stylesheet" href="owl-carousel/owl.theme.css">

        <link rel="stylesheet" href="style.css">



    </head>

    <body>


