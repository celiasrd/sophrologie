<footer>

    <nav class="footer">

        <ul>
            <li><a href="quisuije.php">Qui suis-je?</a></li>
            <li><a href="#">Le Cabinet</a></li>
            <li><a href="#">Présentation de la sophrologie</a></li>
            <li><a href="#">Évènements</a></li>
            <li><a href="contact.php">Contact</a></li>
        </ul>

        <ul>
            <li><a class="fb hidden_text"></a></li>
            <li><a class="google hidden_text"></a></li>
        </ul>
        
        
    </nav>

</footer>




 <script src="assets/js/jquery-1.9.1.min.js"></script>
<!-- Include js plugin -->
<script src="owl-carousel/owl.carousel.js"></script>

<script>

$(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
 
      slideSpeed : 300,
      autoPlay : 3000,
      paginationSpeed : 500,
      singleItem:true,
 
  });
 
});

</script>


</body>  
</html>