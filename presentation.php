
<?php
require_once 'database.php';

$page_title = "Sophrologie - Frédérique Caillet Morel";

require_once 'layout/header.php';
?>

  <nav class="nav_header">

                <ul>
                    <li><a href="index.php"><img src="images/logo.png"></a></li>
                    <li><a href="quisuije.php">Qui suis-je?</a></li>
                    <li><a href="#">Le Cabinet</a></li>
                    <li><a href="presentation.php">Présentation de la sophrologie</a></li>
                    <li><a href="evenement.php">Évènements</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>


            </nav>


<div class="imageheader2">
    <h1 id="quisuije"> la sophrologie</h1>
</div>


<section>
    <article id="images">
        <img class="taille" src="images/merprez.png">
        <img class="taille" src="images/yogaprez.png">
        <img class="taille"src="images/merprez.png">
    </article>
    
    
</section>


<section id="bloc">
    <div>
    <h1 id="titreyoga">essential for everyday yoga</h1>
    <p id="txtsophro">La sophrologie propose à toute personne qui le désire d’améliorer sa qualité de vie, de préserver son capital santé, grâce à des techniques de relaxation spécifiques accessibles à tous :
        <br><br>
LA RESPIRATION : 
permet un lâcher prise, diminuee le niveau d’anxiété, le rythme cardiaque pour atteindre une détente 
salvatrice.
<br>
<br>
LA CORPORALITÉ : 
prendre conscience de son corps dans son intégralité comme lieu de présencen en y associant le 
mouvement puis le relâchement. Tendre à libérer les tensions corporelles et émotionelles.
<br>
<br>
LA VISUALISATION : 
permet de renouer avec notre imaginaire, de dynamiser nos ressources et pensées positives. Elle renforce notre attention, nos capacités qualités et valeurs personnelles afin de vivre les situations de la vie avec davantage de  sérénité.
<br>
<br>
Sos = Harmonie, sérénité physique et mentale
Phren = Conscience, esprit
Logos = Etude, science

    </p>
    
    </div>


</section>

<?php require_once 'layout/footer.php';?>
