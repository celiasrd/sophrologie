
<?php
require_once 'database.php';

$page_title = "Sophrologie - Frédérique Caillet Morel";

require_once 'layout/header.php';
?>

  <nav class="nav_header">

                
    <ul>
        <li><a href="index.php"><img src="images/logo.png"></a></li>
        <li><a href="quisuije.php">Qui suis-je?</a></li>
        <li><a href="cabinet.php">Le Cabinet</a></li>
        <li><a href="presentation.php">Présentation de la sophrologie</a></li>
        <li><a href="evenement.php">Évènements</a></li>
        <li><a href="contact.php">Contact</a></li>
    </ul>
            </nav>


<div class="imageheader-evenement">
    <h1 id="quisuije">nos évènements</h1>
</div>


<img id="tailleevent" src="images/photo-event.png">

<section id="blocevent">
    <section>

    <div class="evenement_home">

      
        
        <div class="evenement">
        
        <article>
            <h3>Évènement 1</h3>
            <p>PETIT TEXTE LOREM IPSUM
AFIN DE DECRIRE VOTRE
EVENNEMENT LOREM IPSUM</p>
            
            <p class="date">LUNDI 3 NOVEMBRE
12h-13h </p>
        </article>
        
        <article>
            <h3>Évènement 3</h3>
            <p>PETIT TEXTE LOREM IPSUM
AFIN DE DECRIRE VOTRE
EVENNEMENT LOREM IPSUM</p>
            
            <p class="date">LUNDI 3 NOVEMBRE
12h-13h </p>
        </article>
        
        <article>
            <h3>Évènement 3</h3>
            <p>PETIT TEXTE LOREM IPSUM
AFIN DE DECRIRE VOTRE
EVENNEMENT LOREM IPSUM</p>
            
            <p class="date">LUNDI 3 NOVEMBRE
12h-13h </p>
        </article>
            
            
            <article>
            <h3>Évènement 1</h3>
            <p>PETIT TEXTE LOREM IPSUM
AFIN DE DECRIRE VOTRE
EVENNEMENT LOREM IPSUM</p>
            
            <p class="date">LUNDI 3 NOVEMBRE
12h-13h </p>
        </article>
            
            <a href="precedent" class="precedent">Précédent</a>
              <a href=suivant" class="suivant">Suivant</a>
            
        </div>

    </div>


</section>
    
    
</section>

<?php require_once 'layout/footer.php'; ?>